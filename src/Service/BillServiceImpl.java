package Service;

import Model.Bill;
import Utils.MySQLConnUtils;

import javax.servlet.RequestDispatcher;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public class BillServiceImpl implements BillService {
    @Override
    public List<Bill> findAll() throws SQLException, ClassNotFoundException {
        String sql = "select a.id_bill a.time_enter a.time_out a.id_employee a.id_table from bill a";
        Connection cnn = MySQLConnUtils.getSqlConnection();
        PreparedStatement ps = cnn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        while (rs.next()){
            rs.getInt("id_bill");
            rs.getString("time_enter");
            rs.getString("time_out");
            rs.getInt("id_employee");
        }
        return null;
    }

    @Override
    public void save(Bill bill) {

    }

    @Override
    public Bill findById(int id) {
        return null;
    }

    @Override
    public void update(int id, Bill bill) {

    }

    @Override
    public void remove(int id) {

    }
}
