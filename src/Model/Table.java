package Model;

public class Table {
    private int id_table;
    private String local;
    private int capacity;
    private boolean is_available;
    public Table(){

    }
    public Table(int id_table,String local,int capacity, boolean is_available){
        this.id_table=id_table;
        this.local=local;
        this.capacity=capacity;
        this.is_available=is_available;
    }

    public int getId_table() {
        return id_table;
    }

    public void setId_table(int id_table) {
        this.id_table = id_table;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public boolean isIs_available() {
        return is_available;
    }

    public void setIs_avaiable(boolean is_available) {
        this.is_available = is_available;
    }
}
