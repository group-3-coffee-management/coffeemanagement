package Model;

import java.util.Date;

public class Bill {
    private int id_bill;
    private Date time_enter;
    private Date time_out;
    private int id_employee;
    private int id_table;

    public Bill(int id_bill, Date time_enter, Date time_out, int id_employee, int id_table) {
        this.id_bill = id_bill;
        this.time_enter = time_enter;
        this.time_out = time_out;
        this.id_employee = id_employee;
        this.id_table = id_table;
    }

    public int getId_bill() {
        return id_bill;
    }

    public void setId_bill(int id_bill) {
        this.id_bill = id_bill;
    }

    public Date getTime_enter() {
        return time_enter;
    }

    public void setTime_enter(Date time_enter) {
        this.time_enter = time_enter;
    }

    public Date getTime_out() {
        return time_out;
    }

    public void setTime_out(Date time_out) {
        this.time_out = time_out;
    }

    public int getId_employee() {
        return id_employee;
    }

    public void setId_employee(int id_employee) {
        this.id_employee = id_employee;
    }

    public int getId_table() {
        return id_table;
    }

    public void setId_table(int id_table) {
        this.id_table = id_table;
    }
}
