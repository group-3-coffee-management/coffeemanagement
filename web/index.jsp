<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: PhanAnhVu
  Date: 11/11/2019
  Time: 2:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="Support/css/login.css"/>
<%--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"--%>
<%--          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"--%>
<%--          crossorigin="anonymous">--%>
<%--    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"--%>
<%--            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"--%>
<%--            crossorigin="anonymous"></script>--%>
<%--    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"--%>
<%--            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"--%>
<%--            crossorigin="anonymous"></script>--%>
<%--    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"--%>
<%--            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"--%>
<%--            crossorigin="anonymous"></script>--%>
  </head>
  <body>
    <form method="post" action="login">
      <div class="login">
        <div class="login__row">
          <div class="login__cell">
            Id
          </div>
          <div class="login__cell">
            <input type="number" name="id_employee" id="id_employee">
          </div>
        </div>
        <div class="login__row">
          <div class="login__cell">Password</div>
          <div class="login__cell"><input type="password" name="password_id_employee" id="password_id_employee"></div>
        </div>
        <div class="login__cell">
          <input type="submit" value="Đăng Nhập"><c:if test="${sessionScope['login_info']==null}">
          <span>${requestScope['messenger']}</span>
        </c:if>
        </div>
      </div>
    </form>
  </body>
</html>
